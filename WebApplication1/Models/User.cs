﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class User
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string DateOfBirth { get; set; }
        public UserRole Role { get; set; }
        public bool LoggedIn { get; set; }
        public double Points { get; set; }
        public UsertType Type { get; set; }
        public bool Deleted { get; set; }
        public int Cc { get; set; }



        public User()
        {
            Username = "";
            Password = "";
            Name = "";
            LastName = "";
            Gender = "";
            DateOfBirth = "";
            LoggedIn = false;
            Points = 0;
            Deleted = false;
        }

        public User(string username, string password, string name, string lastname, string gender, string dateofbirth, UserRole role, double points, UsertType type)
        {
            Username = username;
            Password = password;
            Name = name;
            LastName = lastname;
            Gender = gender;
            DateOfBirth = dateofbirth;
            Role = role;
            LoggedIn = false;
            Points = points;
            Type = type;
            Deleted = false;
            Cc = 0;
        }
    }
}