﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Ticket
    {
        public string Id { get; set; }
        public string EventName { get; set; }
        public string DateTimeEvent { get; set; }
        public double Price { get; set; }
        public string BuyerName { get; set; }
        public string BuyerLastName { get; set; }
        public TicketStatus Status { get; set; }
        public TicketType Type { get; set; }
        public int Quantity { get; set; }
        public string EventBelongs { get; set; }
        public bool Deleted { get; set; }



        public Ticket()
        {
            Id = "";
            EventName = "";
            DateTimeEvent = "";
            Price = 0;
            BuyerName = "";
            BuyerLastName = "";
            Quantity = 0;
            EventBelongs = "";
            Deleted = false;
        }

        public Ticket(string id, string eventname, string datetimeevent, double price, string buyername, string buyerlastname, TicketStatus status, TicketType type, int quantity, string eventbelongs)
        {
            Id = id;
            EventName = eventname;
            DateTimeEvent = datetimeevent;
            Price = price;
            BuyerName = buyername;
            BuyerLastName = buyerlastname;
            Status = status;
            Type = type;
            Quantity = quantity;
            EventBelongs = eventbelongs;
            Deleted = false;
        }


    }
}