﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class TicketHelp
    {
        public TicketHelp()
        {
        }

        public TicketHelp(string name, string dateTimeEvent, double price, double price1)
        {
            Name = name;
            DateTimeEvent = dateTimeEvent;
            Price = price;
            Price1 = price1;
        }

        public string Name { get; set; }
        public string DateTimeEvent { get; set; }
        public double Price { get; set; }
        public double Price1 { get; set; }

        
    }
}