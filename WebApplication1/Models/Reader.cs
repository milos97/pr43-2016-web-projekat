﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace WebApplication1.Models
{
    public class Reader
    {
        public Dictionary<string,User> ReadUsers(string path)
        {
            Dictionary<string,User> users = new Dictionary<string, User>();
            path = HostingEnvironment.MapPath(path);
            
            using(StreamReader reader = new StreamReader(path))
            {
                while (reader.EndOfStream == false)
                {
                    string[] tokens =reader.ReadLine().Split(',');

                    if(tokens[0] != "")
                    {
                        User u = new User();
                        string un = tokens[0];
                        u.Username = tokens[0];
                        u.Password = tokens[1];
                        u.Name = tokens[2];
                        u.LastName = tokens[3];
                        u.Gender = tokens[4];
                        u.DateOfBirth = tokens[5];
                        switch (tokens[6])
                        {
                            case "ADMINISTRATOR":
                                u.Role = UserRole.ADMINISTRATOR;
                                break;
                            case "SELLER":
                                u.Role = UserRole.SELLER;
                                break;
                            case "GUEST":
                                u.Role = UserRole.GUEST;
                                break;
                            case "REGISTRATED":
                                u.Role = UserRole.REGISTRATED;
                                break;
                            default: break;
                        }

                        u.Points = double.Parse(tokens[7]);
                        switch(tokens[8])
                        {
                            case "BRONZE":
                                u.Type = UsertType.BRONZE;
                                break;
                            case "SILVER":
                                u.Type = UsertType.SILVER;
                                break;
                            case "GOLD":
                                u.Type = UsertType.GOLD;
                                break;
                            default:break;
                        }

                        u.Deleted = bool.Parse(tokens[9]);
                        u.Cc = Int32.Parse(tokens[10]);

                        users.Add(un, u);
                    }

                    
                }
            }
           
            
            return users;
        }


        public Dictionary<string, Event> ReadEvents(string path)
        {
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            path = HostingEnvironment.MapPath(path);

            using (StreamReader reader = new StreamReader(path))
            {
                while (reader.EndOfStream == false)
                {
                    string[] tokens = reader.ReadLine().Split(',');

                    if (tokens[0] != "")
                    {
                        Event e = new Event();
                        string n = tokens[0];
                        e.Name = tokens[0];
                        switch(tokens[1])
                        {
                            case "CONCERT":
                                e.Type = EventType.CONCERT;
                                break;
                            case "FESTIVAL":
                                e.Type = EventType.FESTIVAL;
                                break;
                            case "SPORTS_GAME":
                                e.Type = EventType.SPORTS_GAME;
                                break;
                            default:break;
                        }
                        e.NumberOfSeats = Int32.Parse(tokens[2]);
                        e.DateTime = tokens[3];
                        e.PriceRegular = Double.Parse(tokens[4]);
                        e.Location = tokens[5];
                        e.Picture = tokens[6];

                        switch(tokens[7])
                        {
                            case "ACTIVE":
                                e.Status = EventStatus.ACTIVE;
                                break;
                            case "INACTIVE":
                                e.Status = EventStatus.INACTIVE;
                                break;
                            default:break;
                        }

                        e.Belongs = tokens[8];
                        e.Deleted = bool.Parse(tokens[9]);
                        e.Venue = tokens[10];

                        events.Add(n, e);
                    }


                }
            }


            return events;
        }

        public Dictionary<string, Ticket> ReadReservations(string path)
        {
            Dictionary<string, Ticket> reservations = new Dictionary<string, Ticket>();
            path = HostingEnvironment.MapPath(path);

            using(StreamReader reader = new StreamReader(path))
            {
                while(reader.EndOfStream == false)
                {
                    string[] tokens = reader.ReadLine().Split(',');
                    
                    if(tokens[0] != "")
                    {
                        Ticket t = new Ticket();
                        string id = tokens[0];
                        t.Id = tokens[0];
                        t.EventName = tokens[1];
                        t.DateTimeEvent = tokens[2];
                        t.Price = double.Parse(tokens[3]);
                        t.BuyerName = tokens[4];
                        t.BuyerLastName = tokens[5];
                        switch(tokens[6])
                        {
                            case "RESERVED":
                                t.Status = TicketStatus.RESERVED;
                                break;
                            case "CANCELED":
                                t.Status = TicketStatus.CANCELED;
                                break;
                            default:break;
                        }

                        switch(tokens[7])
                        {
                            case "REGULAR":
                                t.Type = TicketType.REGULAR;
                                break;
                            case "FANPIT":
                                t.Type = TicketType.FANPIT;
                                break;
                            case "VIP":
                                t.Type = TicketType.VIP;
                                break;
                            default:break;
                        }

                        t.Quantity = Int32.Parse(tokens[8]);
                        t.EventBelongs = tokens[9];
                        t.Deleted = bool.Parse(tokens[10]);
                        reservations.Add(id, t);
                    }
                }
            }

            return reservations;

        }

        public Dictionary<string, Comment> ReadComments(string path)
        {
            Dictionary<string, Comment> comments = new Dictionary<string, Comment>();
            path = HostingEnvironment.MapPath(path);

            using (StreamReader reader = new StreamReader(path))
            {
                while (reader.EndOfStream == false)
                {
                    string[] tokens = reader.ReadLine().Split(',');

                    if (tokens[0] != "")
                    {
                        Comment c = new Comment();
                        c.BuyerName = tokens[0];
                        c.BuyerLastName = tokens[1];
                        c.EventName = tokens[2];
                        c.Text = tokens[3];
                        c.Grade = double.Parse(tokens[4]);
                        switch (tokens[5])
                        {
                            case "APPROVED":
                                c.Status = CommentStatus.APPROVED;
                                break;
                            case "REFUSED":
                                c.Status = CommentStatus.REFUSED;
                                break;
                            default:break;
                        }
                        c.Id = tokens[6];
                        c.Deleted = bool.Parse(tokens[7]);

                        comments.Add(c.Id, c);
                    }
                }
            }

            return comments;

        }
    }
}