﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Comment
    {
        public Comment()
        {
        }

        public Comment(string buyerName, string buyerLastName, string eventName, string text, double grade, CommentStatus status, string id)
        {
            BuyerName = buyerName;
            BuyerLastName = buyerLastName;
            EventName = eventName;
            Text = text;
            Grade = grade;
            Status = status;
            Id = id;
            Deleted = false;
        }

        public string BuyerName { get; set; }
        public string BuyerLastName { get; set; }
        public string EventName { get; set; }
        public string Text { get; set; }
        public double Grade { get; set; }
        public CommentStatus Status { get; set; }
        public string Id { get; set; }
        public bool Deleted { get; set; }

    }
}