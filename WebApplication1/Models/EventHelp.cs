﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class EventHelp
    {
        public EventHelp()
        {
        }

        public EventHelp(string name, string location, double price, double price1, string date,string venue)
        {
            Name = name;
            Location = location;
            Price = price;
            Price1 = price1;
            Date = date;
            Venue = venue;
        }

        public string Name { get; set; }
        public string Location { get; set; }
        public double Price { get; set; }
        public double Price1 { get; set; }
        public string Date { get; set; }
        public string Venue { get; set; }


    }
}