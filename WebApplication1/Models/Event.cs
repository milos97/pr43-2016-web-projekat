﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.Models
{
    public class Event
    {
        public string Name { get; set; }
        public EventType Type { get; set; }
        public int NumberOfSeats { get; set; }
        public string DateTime { get; set; }
        public double PriceRegular { get; set; }
        public string Location { get; set; }
        public string Picture { get; set; }
        public EventStatus Status { get; set; }
        public string Belongs { get; set; }
        public bool Deleted { get; set; }

        public string Venue { get; set; }

        public Event()
        {
            Name = "";
            NumberOfSeats = 0;
            DateTime = "";
            PriceRegular = 0;
            Location = "";
            Picture = "";
            Belongs = "";
            Deleted = false;
            Venue = "";
        }

        public Event(string name, EventType type, int numberofseats, string datetime, double priceregular, string location, EventStatus status, string belongs,string venue)
        {
            Name = name;
            Type = type;
            NumberOfSeats = numberofseats;
            DateTime = datetime;
            PriceRegular = priceregular;
            Location = location;
            Status = status;
            Belongs = belongs;
            Deleted = false;
            Venue = venue;
        }

    }
}