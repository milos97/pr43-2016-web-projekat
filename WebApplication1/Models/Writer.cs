﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Writer
    {
        public void WriteUserToFile(Dictionary<string, User> someDict, string path)
        {
            using (StreamWriter fileWriter = new StreamWriter(path, append:false))
            {
                foreach (KeyValuePair<string, User> kvPair in someDict)
                {
                    fileWriter.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}", kvPair.Key, kvPair.Value.Password, kvPair.Value.Name,
                       kvPair.Value.LastName, kvPair.Value.Gender, kvPair.Value.DateOfBirth, kvPair.Value.Role, kvPair.Value.Points, kvPair.Value.Type, kvPair.Value.Deleted,kvPair.Value.Cc);
                }
                fileWriter.Close();
            }
        }

        public void WriteEventToFile(Dictionary<string, Event> pairs, string path, string sellername)
        {
            using(StreamWriter fileWriter = new StreamWriter(path, append:true))
            {
                foreach (KeyValuePair<string, Event> item in pairs)
                {
                    fileWriter.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}", item.Key, item.Value.Type, item.Value.NumberOfSeats, 
                        item.Value.DateTime, item.Value.PriceRegular, item.Value.Location, item.Value.Picture, item.Value.Status, sellername, item.Value.Deleted, item.Value.Venue);
                }

                fileWriter.Close();
            }
        }

        public void ChangeStatus(Dictionary<string, Event> pairs, string path, string sellername)
        {
            using (StreamWriter fileWriter = new StreamWriter(path, append: false))
            {
                foreach (KeyValuePair<string, Event> item in pairs)
                {
                    fileWriter.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}", item.Key, item.Value.Type, item.Value.NumberOfSeats,
                        item.Value.DateTime, item.Value.PriceRegular, item.Value.Location, item.Value.Picture, item.Value.Status, item.Value.Belongs,item.Value.Deleted, item.Value.Venue);
                }

                fileWriter.Close();
            }
        }

        public void WriteReservationToFile(Ticket ticket, string path)
        {
            using(StreamWriter fileWriter = new StreamWriter(path, append:true))
            {
                fileWriter.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}", Guid.NewGuid(), ticket.EventName, ticket.DateTimeEvent, ticket.Price, ticket.BuyerName,
                    ticket.BuyerLastName, ticket.Status, ticket.Type, ticket.Quantity, ticket.EventBelongs,ticket.Deleted);
                fileWriter.Close();
            }
        }

        public void ChangeRes(Dictionary<string,Ticket> pairs, string path)
        {
            using (StreamWriter fileWriter = new StreamWriter(path, append: false))
            {
                foreach (var item in pairs)
                {
                    fileWriter.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}", item.Value.Id, item.Value.EventName, item.Value.DateTimeEvent, item.Value.Price, item.Value.BuyerName,
                    item.Value.BuyerLastName, item.Value.Status, item.Value.Type, item.Value.Quantity, item.Value.EventBelongs,item.Value.Deleted);
                    
                }

                fileWriter.Close();
            }
        }

        public void WriteCommentToFile(Comment comment, string path)
        {
            using (StreamWriter fileWriter = new StreamWriter(path, append: true))
            {
                fileWriter.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7}", comment.BuyerName,comment.BuyerLastName,comment.EventName,comment.Text,comment.Grade,comment.Status, Guid.NewGuid(),comment.Deleted);
                fileWriter.Close();
            }
        }

        public void ChangeCom(Dictionary<string, Comment> pairs, string path)
        {
            using (StreamWriter fileWriter = new StreamWriter(path, append: false))
            {
                foreach (var item in pairs)
                {
                    fileWriter.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7}", item.Value.BuyerName, item.Value.BuyerLastName, item.Value.EventName,
                        item.Value.Text, item.Value.Grade, item.Value.Status, item.Value.Id, item.Value.Deleted);

                }

                fileWriter.Close();
            }
        }


    }
}