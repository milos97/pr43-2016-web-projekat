﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class ReservationController : Controller
    {
        public static Dictionary<string, Ticket> sort = new Dictionary<string, Ticket>();

        // GET: Reservation
        public ActionResult Index(string name)
        {
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            Reader r = new Reader();
            events = r.ReadEvents("~/App_Data/Events.txt");
            Dictionary<string, Event> help = new Dictionary<string, Event>();

            foreach (var item in events)
            {
                if (item.Key == name)
                {
                    help[item.Key] = events[item.Key];
                }
            }

            TempData["event"] = help;
            ViewBag.Event = help;
            ViewBag.Msg = TempData["seats"];

            return View();
        }

        [HttpPost]
        public ActionResult Reserve(Ticket ticket)
        {
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            Reader r = new Reader();
            events = r.ReadEvents("~/App_Data/Events.txt");
            Dictionary<string, Event> help = new Dictionary<string, Event>();
            help = (Dictionary<string, Event>)TempData["event"];
            User user = (User)Session["user"];
            Event e = new Event();
            Event ne = new Event();

            foreach (var item in help)
            {
                e.Name = item.Value.Name;
                e.NumberOfSeats = item.Value.NumberOfSeats;
                e.DateTime = item.Value.DateTime;
                e.Location = item.Value.Location;
                e.Picture = item.Value.Picture;
                e.PriceRegular = item.Value.PriceRegular;
                e.Status = item.Value.Status;
                e.Type = item.Value.Type;
                e.Belongs = item.Value.Belongs;
            }

            ticket.EventName = e.Name;
            ticket.DateTimeEvent = e.DateTime;
            ticket.BuyerName = user.Name;
            ticket.BuyerLastName = user.LastName;
            ticket.Status = TicketStatus.RESERVED;
            ticket.EventBelongs = e.Belongs;

            if(ticket.Type.ToString() == "REGULAR")
            {
                ticket.Price = e.PriceRegular * ticket.Quantity;
            }
            else if(ticket.Type.ToString() == "FANPIT")
            {
                ticket.Price = e.PriceRegular * ticket.Quantity * 2;
            }
            else if(ticket.Type.ToString() == "VIP")
            {
                ticket.Price = e.PriceRegular * ticket.Quantity * 4;
            }

            if(user.Type.ToString() == "SILVER")
            {
                ticket.Price = ticket.Price - (ticket.Price * 5 / 100);
            }
            else if(user.Type.ToString() == "GOLD")
            {
                ticket.Price = ticket.Price - (ticket.Price * 10 / 100);
            }
            

            ne = e;
            
            TempData["ticket"] = ticket;
            TempData["new"] = ne;
            TempData["old"] = e;

            return RedirectToAction("Confirm", "Reservation");
        }

        public ActionResult Confirm()
        {
            ViewBag.Ticket = TempData["ticket"];
            TempData["ticket1"] = ViewBag.Ticket;

            return View();
        }

        public ActionResult ConfirmReservation()
        {
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            Reader r = new Reader();
            events = r.ReadEvents("~/App_Data/Events.txt");
            Ticket ticket = (Ticket)TempData["ticket1"];
            Event e = (Event)TempData["old"];
            Event ne = (Event)TempData["new"];
            User user = (User)Session["user"];
            User user1 = user;
            Dictionary<string, User> users = new Dictionary<string, User>();
            users = r.ReadUsers("~/App_Data/Users.txt");

            if (ne.NumberOfSeats > ticket.Quantity)
            {
                ne.NumberOfSeats = ne.NumberOfSeats - ticket.Quantity;
            }
            else
            {
                TempData["seats"] = "We don't have that many available seats!";
                return RedirectToAction("Index", "Reservation");
            }

            user1.Points = user1.Points + ticket.Price / 1000 * 133;
            if(user1.Points >= 5000)
            {
                user1.Type = UsertType.SILVER;
            }
            if(user1.Points >= 10000)
            {
                user1.Type = UsertType.GOLD;
            }

            users.Remove(user.Username);
            users.Add(user1.Username, user1);

            events.Remove(e.Name);
            events.Add(ne.Name, ne);

            Writer w = new Writer();
            w.WriteUserToFile(users, "C:/Users/Administrator/Desktop/Web/WebApplication1/WebApplication1/App_Data/Users.txt");
            w.ChangeStatus(events, "C:/Users/Administrator/Desktop/Web/WebApplication1/WebApplication1/App_Data/Events.txt", ne.Belongs);
            w.WriteReservationToFile(ticket, "C:/Users/Administrator/Desktop/Web/WebApplication1/WebApplication1/App_Data/Reservation.txt");

            return RedirectToAction("RegHome", "Register");
        }

        public ActionResult SeeReservations()
        {
            Dictionary<string, Ticket> reservations = new Dictionary<string, Ticket>();
            Reader r = new Reader();
            reservations = r.ReadReservations("~/App_Data/Reservation.txt");
            ViewBag.Reservation = reservations;

            return View();
        }

        public ActionResult CancelReservation(string id)
        {
            Dictionary<string, Ticket> reservations = new Dictionary<string, Ticket>();
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            Reader r = new Reader();
            reservations = r.ReadReservations("~/App_Data/Reservation.txt");
            events = r.ReadEvents("~/App_Data/Events.txt");
            Ticket t = new Ticket();
            Event ne = new Event();
            User user = (User)Session["user"];
            Dictionary<string, User> users = new Dictionary<string, User>();
            users = r.ReadUsers("~/App_Data/Users.txt");

            foreach (var item in reservations)
            {
                if(item.Value.Id == id)
                {
                    t.Id = item.Value.Id;
                    t.Price = item.Value.Price;
                    t.Quantity = item.Value.Quantity;
                    t.Status = item.Value.Status;
                    t.Type = item.Value.Type;
                    t.EventName = item.Value.EventName;
                    t.DateTimeEvent = item.Value.DateTimeEvent;
                    t.BuyerName = item.Value.BuyerName;
                    t.BuyerLastName = item.Value.BuyerLastName;
                    t.EventBelongs = item.Value.EventBelongs;
                    
                }
            }

            foreach (var item in events)
            {
                if(item.Value.Name == t.EventName)
                {
                    ne.Name = item.Value.Name;
                    ne.NumberOfSeats = item.Value.NumberOfSeats;
                    ne.DateTime = item.Value.DateTime;
                    ne.Location = item.Value.Location;
                    ne.Picture = item.Value.Picture;
                    ne.PriceRegular = item.Value.PriceRegular;
                    ne.Status = item.Value.Status;
                    ne.Type = item.Value.Type;
                    ne.Belongs = item.Value.Belongs;
                }
            }

            events.Remove(ne.Name);
            ne.NumberOfSeats = ne.NumberOfSeats + t.Quantity;
            events.Add(ne.Name, ne);
            users.Remove(user.Username);
            user.Points = user.Points - t.Price / 1000 * 133 * 4;
            if(user.Points < 5000)
            {
                user.Type = UsertType.BRONZE;
            }

            if(user.Points >= 5000 && user.Points < 10000)
            {
                user.Type = UsertType.SILVER;
            }
            user.Cc = user.Cc + 1;
            users.Add(user.Username, user);
            reservations.Remove(t.Id);
            t.Status = TicketStatus.CANCELED;
            reservations.Add(t.Id, t);

            Writer w = new Writer();
            w.WriteUserToFile(users, "C:/Users/Administrator/Desktop/Web/WebApplication1/WebApplication1/App_Data/Users.txt");
            w.ChangeStatus(events, "C:/Users/Administrator/Desktop/Web/WebApplication1/WebApplication1/App_Data/Events.txt", ne.Belongs);
            w.ChangeRes(reservations, "C:/Users/Administrator/Desktop/Web/WebApplication1/WebApplication1/App_Data/Reservation.txt");

            return RedirectToAction("RegHome", "Register");
        }

        public ActionResult SearchReservation()
        {
            ViewBag.Message = TempData["message1"];
            ViewBag.Msg = TempData["msg1"];
            ViewBag.Reservations = TempData["reservation"];
            return View();
        }

        [HttpPost]
        public ActionResult SearchRes(TicketHelp tk)
        {
            Dictionary<string, Ticket> reservations = new Dictionary<string, Ticket>();
            Reader r = new Reader();
            reservations = r.ReadReservations("~/App_Data/Reservation.txt");
            Dictionary<string, Ticket> help = new Dictionary<string, Ticket>();

            foreach (var item in reservations)
            {
                if(tk.Name == null && tk.Price == 0 && tk.Price1 == 0 && tk.DateTimeEvent == null)
                {
                    TempData["msg1"] = "You have to put some input information";
                }

                else if((item.Value.EventName == tk.Name || tk.Name == null)&&(item.Value.Price >= tk.Price || tk.Price == 0)&&(item.Value.Price <= tk.Price1 || tk.Price1 == 0)&&(item.Value.DateTimeEvent == tk.DateTimeEvent || tk.DateTimeEvent == null))  
                {
                    help[item.Key] = reservations[item.Key];
                } 
               
            }
            if (tk.Name != null || tk.Price != 0 || tk.Price1 != 0 || tk.DateTimeEvent != null)
            {
                if (help.Count == 0)
                {
                    TempData["message1"] = "No match!";
                }
            }
                

            sort = help;
            TempData["reservation"] = help;

            return RedirectToAction("SearchReservation", "Reservation");
        }

        [HttpPost]
        public ActionResult SortRes(string word)
        {
            Dictionary<string, Ticket> help = new Dictionary<string, Ticket>();

            if (word == "ByNameDescending")
            {
                foreach (var item in sort.OrderByDescending(key => key.Value.EventName))
                {
                    help[item.Key] = sort[item.Key];
                }
            }

            else if (word == "ByNameAscending")
            {
                foreach (var item in sort.OrderBy(key => key.Value.EventName))
                {
                    help[item.Key] = sort[item.Key];
                }
            }

            else if (word == "ByDateTimeDescending")
            {
                foreach (var item in sort.OrderByDescending(key => key.Value.DateTimeEvent))
                {
                    help[item.Key] = sort[item.Key];
                }
            }

            else if (word == "ByDateTimeAscending")
            {
                foreach (var item in sort.OrderBy(key => key.Value.DateTimeEvent))
                {
                    help[item.Key] = sort[item.Key];
                }
            }

            else if (word == "ByPriceDescending")
            {
                foreach (var item in sort.OrderByDescending(key => key.Value.Price))
                {
                    help[item.Key] = sort[item.Key];
                }
            }

            else if (word == "ByPriceAscending")
            {
                foreach (var item in sort.OrderBy(key => key.Value.Price))
                {
                    help[item.Key] = sort[item.Key];
                }
            }


            TempData["reservation"] = help;

            return RedirectToAction("SearchReservation", "Reservation");
        }

        [HttpPost]
        public ActionResult FilterRes(string kw)
        {
            Dictionary<string, Ticket> help = new Dictionary<string, Ticket>();

            if(kw == "ShowVipTickets")
            {
                foreach (var item in sort)
                {
                    if(item.Value.Type.ToString() == "VIP")
                    {
                        help[item.Key] = sort[item.Key];
                    }
                }
            }

            if (kw == "ShowRegularTickets")
            {
                foreach (var item in sort)
                {
                    if (item.Value.Type.ToString() == "REGULAR")
                    {
                        help[item.Key] = sort[item.Key];
                    }
                }
            }

            if (kw == "ShowFanpitTickets")
            {
                foreach (var item in sort)
                {
                    if (item.Value.Type.ToString() == "FANPIT")
                    {
                        help[item.Key] = sort[item.Key];
                    }
                }
            }

            if (kw == "ShowReservedTickets")
            {
                foreach (var item in sort)
                {
                    if (item.Value.Status.ToString() == "RESERVED")
                    {
                        help[item.Key] = sort[item.Key];
                    }
                }
            }

            if (kw == "ShowCanceledTickets")
            {
                foreach (var item in sort)
                {
                    if (item.Value.Type.ToString() == "CANCELED")
                    {
                        help[item.Key] = sort[item.Key];
                    }
                }
            }

            if (help.Count == 0)
            {
                TempData["message1"] = "No match!";
            }

            TempData["reservation"] = help;

            return RedirectToAction("SearchReservation", "Reservation");
        }

        public ActionResult Delete(string id)
        {
            Dictionary<string, Ticket> reservations = new Dictionary<string, Ticket>();
            Reader r = new Reader();
            reservations = r.ReadReservations("~/App_Data/Reservation.txt");
            Ticket t = new Ticket();

            foreach (var item in reservations)
            {
                if (item.Value.Id == id)
                {
                    t.Id = item.Value.Id;
                    t.Price = item.Value.Price;
                    t.Quantity = item.Value.Quantity;
                    t.Status = item.Value.Status;
                    t.Type = item.Value.Type;
                    t.EventName = item.Value.EventName;
                    t.DateTimeEvent = item.Value.DateTimeEvent;
                    t.BuyerName = item.Value.BuyerName;
                    t.BuyerLastName = item.Value.BuyerLastName;
                    t.EventBelongs = item.Value.EventBelongs;

                }
            }

            reservations.Remove(t.Id);
            t.Deleted = true;
            reservations.Add(t.Id, t);

            Writer w = new Writer();
            w.ChangeRes(reservations, "C:/Users/Administrator/Desktop/Web/WebApplication1/WebApplication1/App_Data/Reservation.txt");

            return RedirectToAction("RegHome", "Register");
        }
    }
}