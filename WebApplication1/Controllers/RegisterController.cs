﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class RegisterController : Controller
    {
        public static Dictionary<string, User> sort = new Dictionary<string, User>();

        // GET: Register
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(User user)
        {
            Dictionary<string, User> users = new Dictionary<string, User>();
            Reader reader = new Reader();
            users = reader.ReadUsers("~/App_Data/Users.txt");
            if (!users.ContainsKey(user.Username))
            {
                users.Add(user.Username, user);
                Writer writer = new Writer();
                writer.WriteUserToFile(users, "C:/Users/Administrator/Desktop/Web/WebApplication1/WebApplication1/App_Data/Users.txt");

            }
            else
            {
                TempData["por"] = "Username already taken!";
            }
            
            Session["user"] = user;
            return RedirectToAction("Index", "Home");

        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoginUser(string username, string password)
        {
            Dictionary<string, User> users = new Dictionary<string, User>();
            Reader reader = new Reader();
            users = reader.ReadUsers("~/App_Data/Users.txt");

            foreach (KeyValuePair<string, User> u in users)
            {
                if(u.Key == username && u.Value.Password == password && u.Value.Deleted == false)
                {
                    u.Value.LoggedIn = true;
                    Session["user"] = u.Value;
                    return RedirectToAction("RegHome", "Register");
                }
                else
                {
                    TempData["msg"] = "Your username or password is wrong!";
                }

            }

            return RedirectToAction("Index", "Home");
        }


        public ActionResult RegHome()
        {
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            Reader r = new Reader();
            events = r.ReadEvents("~/App_Data/Events.txt");
            Dictionary<string, Event> help = new Dictionary<string, Event>();

            foreach (var item in events.OrderBy(key => key.Value.DateTime))
            {
                help[item.Key] = events[item.Key];
            }

            ViewBag.Events = help;
            ViewBag.Warning = TempData["warning"];
            ViewBag.Por = TempData["por1"];

            return View();
        }

        public ActionResult Details()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangeDetails(User user)
        {
            Dictionary<string, User> users = new Dictionary<string, User>();
            Reader r = new Reader();
            users = r.ReadUsers("~/App_Data/Users.txt");

            User user1 = new User();
            user1 = (User)Session["user"];

            users.Remove(user1.Username);


            if(user1 != null)
            {
                user1.Name = user.Name;
                user1.Username = user.Username;
                user1.LastName = user.LastName;
                user1.Password = user.Password;
                user1.Gender = user.Gender;
                user1.DateOfBirth = user.DateOfBirth;
                //user1.Points = user.Points;
                //user1.Type = user.Type;
                //user1.Deleted = user.Deleted;
            }

            users.Add(user1.Username,user1);
            Writer w = new Writer();
            w.WriteUserToFile(users, "C:/Users/Administrator/Desktop/Web/WebApplication1/WebApplication1/App_Data/Users.txt");
            Session["user"] = user1;

            return RedirectToAction("RegHome", "Register");
        }


        public ActionResult LogOut()
        {
            User u = new User();
            u = (User)Session["user"];
            u.LoggedIn = false;
            Session["user"] = null;

            return RedirectToAction("Index", "Home");

        }

        public ActionResult AllUsers()
        {
            Dictionary<string, User> users = new Dictionary<string, User>();
            Reader r = new Reader();
            users = r.ReadUsers("~/App_Data/Users.txt");
            ViewBag.Users = users;
            return View();
        }

        public ActionResult AddSeller()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Seller(User user)
        {
            Dictionary<string, User> users = new Dictionary<string, User>();
            Reader reader = new Reader();
            users = reader.ReadUsers("~/App_Data/Users.txt");
            if (!users.ContainsKey(user.Username))
            {
                users.Add(user.Username, user);
                Writer writer = new Writer();
                writer.WriteUserToFile(users, "C:/Users/Administrator/Desktop/Web/WebApplication1/WebApplication1/App_Data/Users.txt");

            }
            else
            {
                TempData["por1"] = "Username already taken!";
            }


            return RedirectToAction("RegHome", "Register");
        } 

        public ActionResult SearchUser()
        {
            ViewBag.User = TempData["item"];
            ViewBag.Message = TempData["message"];
            ViewBag.Msg = TempData["x"];
            return View();
        }

        [HttpPost]
        public ActionResult SearchResult(string keyword)
        {
            Dictionary<string, User> users = new Dictionary<string, User>();
            Reader r = new Reader();
            users = r.ReadUsers("~/App_Data/Users.txt");
            Dictionary<string, User> help = new Dictionary<string, User>();
          
           foreach (var item in users)
           {
               if(keyword == "")
               {
                 TempData["x"] = "You have to put some input information";
               }
                
               else if (item.Value.Username.ToLower() == keyword.ToLower() || item.Value.Name.ToLower() == keyword.ToLower() || item.Value.LastName.ToLower() == keyword.ToLower())
               {
                        help[item.Key] = users[item.Key];
               }

           }

            if(keyword != "")
            {
                if (help.Count == 0)
                {
                    TempData["message"] = "No match!";
                }
            }
            
            sort = help;
            TempData["item"] = help;
            return RedirectToAction("SearchUser", "Register");
        }

        [HttpPost]
        public ActionResult Sort(string word)
        {
            Dictionary<string, User> help = new Dictionary<string, User>();

            if (word == "ByUsernameDescending")
            {
                foreach (var item in sort.OrderByDescending(key => key.Key))
                {
                    help[item.Key] = sort[item.Key];
                }
            }

            else if (word == "ByUsernameAscending")
            {
                foreach (var item in sort.OrderBy(key => key.Key))
                {
                    help[item.Key] = sort[item.Key];
                }
            }

            else if (word == "ByNameDescending")
            {
                foreach (var item in sort.OrderByDescending(key => key.Value.Name))
                {
                    help[item.Key] = sort[item.Key];
                }
            }

            else if (word == "ByNameAscending")
            {
                foreach (var item in sort.OrderBy(key => key.Value.Name))
                {
                    help[item.Key] = sort[item.Key];
                }
            }

            else if (word == "ByLastNameDescending")
            {
                foreach (var item in sort.OrderByDescending(key => key.Value.LastName))
                {
                    help[item.Key] = sort[item.Key];
                }
            }

            else if (word == "ByLastNameAscending")
            {
                foreach (var item in sort.OrderBy(key => key.Value.LastName))
                {
                    help[item.Key] = sort[item.Key];
                }
            }

            else if( word == "ByPointsDescending")
            {
                foreach (var item in sort.OrderByDescending(key => key.Value.Points))
                {
                    help[item.Key] = sort[item.Key];
                }
            }

            else if (word == "ByPointsAscending")
            {
                foreach (var item in sort.OrderBy(key => key.Value.Points))
                {
                    help[item.Key] = sort[item.Key];
                }
            }


            TempData["item"] = help;

            return RedirectToAction("SearchUser", "Register");
        }

        [HttpPost]
        public ActionResult Filter(string kw)
        {
            Dictionary<string, User> help = new Dictionary<string, User>();

            if(kw == "ShowAdmins")
            {
                foreach (var item in sort)
                {
                    if(item.Value.Role.ToString() == "ADMINISTRATOR")
                    {
                        help[item.Key] = sort[item.Key];
                    }
                }
            }

            else if(kw == "ShowRegisters")
            {
                foreach (var item in sort)
                {
                    if (item.Value.Role.ToString() == "REGISTRATED")
                    {
                        help[item.Key] = sort[item.Key];
                    }
                }
            }

            else if (kw == "ShowSellers")
            {
                foreach (var item in sort)
                {
                    if (item.Value.Role.ToString() == "SELLER")
                    {
                        help[item.Key] = sort[item.Key];
                    }
                }
            }

            else if (kw == "ShowBronzeUsers")
            {
                foreach (var item in sort)
                {
                    if(item.Value.Type.ToString() == "BRONZE")
                    {
                        help[item.Key] = sort[item.Key];
                    }
                }
            }

            else if (kw == "ShowSilverUsers")
            {
                foreach (var item in sort)
                {
                    if (item.Value.Type.ToString() == "SILVER")
                    {
                        help[item.Key] = sort[item.Key];
                    }
                }
            }

            else if (kw == "ShowGoldUsers")
            {
                foreach (var item in sort)
                {
                    if (item.Value.Type.ToString() == "GOLD")
                    {
                        help[item.Key] = sort[item.Key];
                    }
                }
            }

            if (help.Count == 0)
            {
                TempData["message"] = "No match!";
            }

            TempData["item"] = help;

            return RedirectToAction("SearchUser", "Register");
        }
        
        public ActionResult Delete(string username)
        {
            Dictionary<string, User> users = new Dictionary<string, User>();
            Reader r = new Reader();
            users = r.ReadUsers("~/App_Data/Users.txt");
            User u = new User();

            foreach (var item in users)
            {
                if(item.Value.Username == username)
                {
                    u.Username = item.Value.Username;
                    u.Type = item.Value.Type;
                    u.Role = item.Value.Role;
                    u.Points = item.Value.Points;
                    u.Password = item.Value.Password;
                    u.Name = item.Value.Name;
                    u.LoggedIn = item.Value.LoggedIn;
                    u.LastName = item.Value.LastName;
                    u.Gender = item.Value.Gender;
                    u.Deleted = item.Value.Deleted;
                    u.DateOfBirth = item.Value.DateOfBirth;
                }
            }

            users.Remove(u.Username);
            u.Deleted = true;
            users.Add(u.Username, u);
            Writer w = new Writer();
            w.WriteUserToFile(users, "C:/Users/Administrator/Desktop/Web/WebApplication1/WebApplication1/App_Data/Users.txt");

            return RedirectToAction("RegHome", "Register");
        }

        public ActionResult ShowCriticalUsers()
        {
            Dictionary<string, User> users = new Dictionary<string, User>();
            Reader r = new Reader();
            users = r.ReadUsers("~/App_Data/Users.txt");
            ViewBag.Users1 = users;
            return View();
        }

        public ActionResult BlockUser(string username)
        {
            Dictionary<string, User> users = new Dictionary<string, User>();
            Reader r = new Reader();
            users = r.ReadUsers("~/App_Data/Users.txt");
            User u = new User();

            foreach (var item in users)
            {
                if (item.Value.Username == username)
                {
                    u.Username = item.Value.Username;
                    u.Type = item.Value.Type;
                    u.Role = item.Value.Role;
                    u.Points = item.Value.Points;
                    u.Password = item.Value.Password;
                    u.Name = item.Value.Name;
                    u.LoggedIn = item.Value.LoggedIn;
                    u.LastName = item.Value.LastName;
                    u.Gender = item.Value.Gender;
                    u.Deleted = item.Value.Deleted;
                    u.DateOfBirth = item.Value.DateOfBirth;
                    u.Cc = item.Value.Cc;
                }
            }

            users.Remove(u.Username);
            u.Deleted = true;
            users.Add(u.Username, u);
            Writer w = new Writer();
            w.WriteUserToFile(users, "C:/Users/Administrator/Desktop/Web/WebApplication1/WebApplication1/App_Data/Users.txt");

            return RedirectToAction("RegHome", "Register");
        }
    }
}