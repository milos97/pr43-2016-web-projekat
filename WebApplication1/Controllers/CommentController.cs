﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class CommentController : Controller
    {
        // GET: Comment
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost] 
        public ActionResult AddComment(Comment comment)
        {
            Dictionary<string, Ticket> reservations = new Dictionary<string, Ticket>();
            Reader r = new Reader();
            reservations = r.ReadReservations("~/App_Data/Reservation.txt");
            Comment help = new Comment();
            Writer w = new Writer();
            Dictionary<string, Comment> c = new Dictionary<string, Comment>();

            foreach (var item in reservations)
            {
                if(item.Value.EventName == comment.EventName && item.Value.BuyerName == comment.BuyerName && item.Value.BuyerLastName == comment.BuyerLastName && item.Value.Status.ToString() == "RESERVED" )
                {
                    help.BuyerName = comment.BuyerName;
                    help.BuyerLastName = comment.BuyerLastName;
                    help.EventName = comment.EventName;
                    help.Text = comment.Text;
                    help.Grade = comment.Grade;
                    help.Status = CommentStatus.REFUSED;
                    c.Add(help.EventName, help);
                    
                }
            }

            if(c.Count == 0)
            {
                TempData["warning"] = "You can't comment this event!";
            }

            if(help.Status.ToString() == "REFUSED")
            {
                w.WriteCommentToFile(help, "C:/Users/Administrator/Desktop/Web/WebApplication1/WebApplication1/App_Data/Comment.txt");
            }

            return RedirectToAction("RegHome", "Register");
        }
        
        public ActionResult ChangeStatusCom(string id)
        {
            Dictionary<string, Comment> comments = new Dictionary<string, Comment>();
            Reader r = new Reader();
            comments = r.ReadComments("~/App_Data/Comment.txt");
            Comment c = new Comment();

            foreach (var item in comments) 
            {
                if(item.Value.Id == id)
                {
                    c.BuyerLastName = item.Value.BuyerLastName;
                    c.BuyerName = item.Value.BuyerName;
                    c.EventName = item.Value.EventName;
                    c.Grade = item.Value.Grade;
                    c.Text = item.Value.Text;
                    c.Id = item.Value.Id;
                    c.Status = item.Value.Status;
                }
            }

            comments.Remove(c.Id);
            c.Status = CommentStatus.APPROVED;
            comments.Add(c.Id, c);

            Writer w = new Writer();
            w.ChangeCom(comments, "C:/Users/Administrator/Desktop/Web/WebApplication1/WebApplication1/App_Data/Comment.txt");

            return RedirectToAction("RegHome", "Register");
        }

        public ActionResult Delete(string id)
        {
            Dictionary<string, Comment> comments = new Dictionary<string, Comment>();
            Reader r = new Reader();
            comments = r.ReadComments("~/App_Data/Comment.txt");
            Comment c = new Comment();

            foreach (var item in comments)
            {
                if (item.Value.Id == id)
                {
                    c.BuyerLastName = item.Value.BuyerLastName;
                    c.BuyerName = item.Value.BuyerName;
                    c.EventName = item.Value.EventName;
                    c.Grade = item.Value.Grade;
                    c.Text = item.Value.Text;
                    c.Id = item.Value.Id;
                    c.Status = item.Value.Status;
                }
            }

            comments.Remove(c.Id);
            c.Deleted = true;
            comments.Add(c.Id, c);

            Writer w = new Writer();
            w.ChangeCom(comments, "C:/Users/Administrator/Desktop/Web/WebApplication1/WebApplication1/App_Data/Comment.txt");

            return RedirectToAction("RegHome", "Register");
        }
    }
}