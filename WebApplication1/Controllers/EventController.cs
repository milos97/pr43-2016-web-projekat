﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class EventController : Controller
    {
        public static Dictionary<string, Event> sort = new Dictionary<string, Event>();

        // GET: Event
        public ActionResult Index()
        {
            ViewBag.Message1 = TempData["msg1"];
            return View();
        }

        [HttpPost]
        public ActionResult Add(Event e)
        {
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            Reader r = new Reader();
            events = r.ReadEvents("~/App_Data/Events.txt");
            Dictionary<string, Event> help = new Dictionary<string, Event>();
            Event newev = new Event();
            newev.Name = e.Name;
            newev.Type = e.Type;
            newev.NumberOfSeats = e.NumberOfSeats;
            newev.DateTime = e.DateTime;
            newev.PriceRegular = e.PriceRegular;
            newev.Location = e.Location;
            newev.Picture = e.Picture;
            newev.Status = e.Status;
            newev.Venue = e.Venue;

            foreach (var item in events)
            {
                if(item.Value.DateTime == newev.DateTime && item.Value.Venue == newev.Venue)
                {
                    TempData["msg1"] = "Event with that time and location already exists!";
                    return RedirectToAction("Index", "Event");
                }
                

            }

            help.Add(newev.Name, newev);
            User user = (User)Session["user"];
            Writer w = new Writer();
            w.WriteEventToFile(help, "C:/Users/Administrator/Desktop/Web/WebApplication1/WebApplication1/App_Data/Events.txt", user.Username);
            return RedirectToAction("RegHome", "Register");
        }


        public ActionResult ChangeStatus(string name)
        {
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            Reader r = new Reader();
            events = r.ReadEvents("~/App_Data/Events.txt");
            Dictionary<string, Event> help = new Dictionary<string, Event>();
            string belongs = "";


            foreach (var item in events)
            {
                if(item.Value.Name == name)
                {
                    item.Value.Status = EventStatus.ACTIVE;
                    belongs = item.Value.Belongs;

                }

            }

            Writer w = new Writer();
            w.ChangeStatus(events, "C:/Users/Administrator/Desktop/Web/WebApplication1/WebApplication1/App_Data/Events.txt", belongs);

            return RedirectToAction("RegHome", "Register");

        }

        public ActionResult SearchEvent()
        {
            ViewBag.Message = TempData["message1"];
            ViewBag.Events = TempData["event"];
            ViewBag.Msg = TempData["war"];

            return View();
        }
        
        [HttpPost]
        public ActionResult Search(EventHelp eh)
        {
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            Reader r = new Reader();
            events = r.ReadEvents("~/App_Data/Events.txt");
            Dictionary<string, Event> help = new Dictionary<string, Event>();

            foreach (var item in events)
            {
                
                if(eh.Name == null && eh.Location == null && eh.Date == null && eh.Price == 0 && eh.Venue == null)
                {
                    TempData["war"] = "You have to put some input information";
                }
                else if((item.Value.Name == eh.Name || eh.Name == null)&& (item.Value.Location == eh.Location || eh.Location == null) && (item.Value.PriceRegular >= eh.Price || eh.Price == 0) && (item.Value.PriceRegular <= eh.Price1 || eh.Price1 == 0) && (item.Value.DateTime == eh.Date || eh.Date == null) && (item.Value.Venue == eh.Venue || eh.Venue == null))
                {
                    help[item.Key] = events[item.Key];
                }
            }

            if (eh.Name != null || eh.Location != null || eh.Date != null || eh.Price != 0 || eh.Venue != null)
            {
                if (help.Count == 0)
                {
                    TempData["message1"] = "No match!";
                }
            }

          

            sort = help;
            TempData["event"] = help;

            return RedirectToAction("SearchEvent", "Event");
        }

        [HttpPost]
        public ActionResult SortEvents(string word)
        {
            Dictionary<string, Event> help = new Dictionary<string, Event>();

            if(word == "ByNameDescending")
            {
                foreach (var item in sort.OrderByDescending(key => key.Key))
                {
                    help[item.Key] = sort[item.Key];
                }
            }

            else if(word == "ByNameAscending")
            {
                foreach (var item in sort.OrderBy(key => key.Key))
                {
                    help[item.Key] = sort[item.Key];
                }
            }

            else if(word == "ByDateTimeDescending")
            {
                foreach (var item in sort.OrderByDescending(key => key.Value.DateTime))
                {
                    help[item.Key] = sort[item.Key];
                }
            }

            else if(word == "ByDateTimeAscending")
            {
                foreach (var item in sort.OrderBy(key => key.Value.DateTime))
                {
                    help[item.Key] = sort[item.Key];
                }
            }

            else if(word == "ByPriceDescending")
            {
                foreach (var item in sort.OrderByDescending(key => key.Value.PriceRegular))
                {
                    help[item.Key] = sort[item.Key];
                }
            }

            else if (word == "ByPriceAscending")
            {
                foreach (var item in sort.OrderBy(key => key.Value.PriceRegular))
                {
                    help[item.Key] = sort[item.Key];
                }
            }

            else if(word == "ByVenueDescending")
            {
                foreach (var item in sort.OrderByDescending(key => key.Value.Venue))
                {
                    help[item.Key] = sort[item.Key];
                }
            }

            else if (word == "ByVenueAscending")
            {
                foreach (var item in sort.OrderBy(key => key.Value.Venue))
                {
                    help[item.Key] = sort[item.Key];
                }
            }

            TempData["event"] = help;

            return RedirectToAction("SearchEvent", "Event");
        }

        [HttpPost]
        public ActionResult FilterEvents(string kw)
        {
            Dictionary<string, Event> help = new Dictionary<string, Event>();

            if(kw == "ShowConcerts")
            {
                foreach (var item in sort)
                {
                    if(item.Value.Type.ToString() == "CONCERT")
                    {
                        help[item.Key] = sort[item.Key];
                    }
                }
            }

            else if(kw == "ShowFestivals")
            {
                foreach (var item in sort)
                {
                    if (item.Value.Type.ToString() == "FESTIVAL")
                    {
                        help[item.Key] = sort[item.Key];
                    }
                }
            }

            else if (kw == "ShowSportGames")
            {
                foreach (var item in sort)
                {
                    if (item.Value.Type.ToString() == "SPORTS_GAME")
                    {
                        help[item.Key] = sort[item.Key];
                    }
                }
            }

            else if(kw == "ShowUnsold")
            {
                foreach (var item in sort)
                {
                    if(item.Value.NumberOfSeats > 0)
                    {
                        help[item.Key] = sort[item.Key];
                    }
                }
            }

            if (help.Count == 0)
            {
                TempData["message1"] = "No match!";
            }


            TempData["event"] = help;
            return RedirectToAction("SearchEvent", "Event");
        }

        public ActionResult Details(string name)
        {
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            Reader r = new Reader();
            events = r.ReadEvents("~/App_Data/Events.txt");
            Dictionary<string, Event> help = new Dictionary<string, Event>();

            foreach (var item in events)
            {
                if(item.Key == name)
                {
                    help[item.Key] = events[item.Key];
                }
            }

            TempData["event"] = help;
            ViewBag.Event = help;

            return View();
        }

        [HttpPost]
        public ActionResult ChangeDetails(Event e)
        {
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            Reader r = new Reader();
            events = r.ReadEvents("~/App_Data/Events.txt");
            Dictionary<string, Event> help = new Dictionary<string, Event>();
            help = (Dictionary<string, Event>)TempData["event"];
            Event old = new Event();
            Event n = new Event();

            foreach (var item in help)
            {
                old.Name = item.Value.Name;
                old.Type = item.Value.Type;
                old.NumberOfSeats = item.Value.NumberOfSeats;
                old.DateTime = item.Value.DateTime;
                old.PriceRegular = item.Value.PriceRegular;
                old.Location = item.Value.Location;
                old.Picture = item.Value.Picture;
                old.Belongs = item.Value.Belongs;
                old.Venue = item.Value.Venue;
                
            }

            n.Name = e.Name;
            n.Type = e.Type;
            n.NumberOfSeats = e.NumberOfSeats;
            n.DateTime = e.DateTime;
            n.PriceRegular = e.PriceRegular;
            n.Location = e.Location;
            n.Picture = e.Picture;
            n.Belongs = old.Belongs;
            n.Venue = e.Venue;

            events.Remove(old.Name);
            events.Add(n.Name, n);

            User user = (User)Session["user"];
            Writer w = new Writer();
            w.ChangeStatus(events, "C:/Users/Administrator/Desktop/Web/WebApplication1/WebApplication1/App_Data/Events.txt", user.Username);
            return RedirectToAction("RegHome", "Register");

        }

        public ActionResult MoreDetails(string name)
        {
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            Reader r = new Reader();
            events = r.ReadEvents("~/App_Data/Events.txt");
            Dictionary<string, Event> help = new Dictionary<string, Event>();
            Dictionary<string, Comment> comments = new Dictionary<string, Comment>();
            comments = r.ReadComments("~/App_Data/Comment.txt");
            Dictionary<string, Comment> c = new Dictionary<string, Comment>();

            foreach (var item in events)
            {
                if (item.Key == name)
                {
                    help[item.Key] = events[item.Key];
                }
            }

            foreach (var item in comments)
            {
                if(item.Value.EventName == name)
                {
                    c[item.Key] = comments[item.Key];
                }
            }

            TempData["event"] = help;
            TempData["comment"] = c;
            ViewBag.Event = help;
            ViewBag.Comment = c;

            return View();
        }

        public ActionResult ShowEvents()
        {
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            Reader r = new Reader();
            events = r.ReadEvents("~/App_Data/Events.txt");

            ViewBag.Ev = events;

            return View();
        }

        public ActionResult Delete(string name)
        {
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            Reader r = new Reader();
            events = r.ReadEvents("~/App_Data/Events.txt");
            Dictionary<string, Event> help = new Dictionary<string, Event>();
            string belongs = "";


            foreach (var item in events)
            {
                if (item.Value.Name == name)
                {
                    item.Value.Deleted = true;
                    belongs = item.Value.Belongs;

                }

            }

            Writer w = new Writer();
            w.ChangeStatus(events, "C:/Users/Administrator/Desktop/Web/WebApplication1/WebApplication1/App_Data/Events.txt", belongs);

            return RedirectToAction("RegHome", "Register");
        }


    }
}