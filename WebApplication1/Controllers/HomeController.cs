﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            Reader r = new Reader();
            events = r.ReadEvents("~/App_Data/Events.txt");
            Dictionary<string, Event> help = new Dictionary<string, Event>();

            foreach (var item in events.OrderBy(key => key.Value.DateTime))
            {
                help[item.Key] = events[item.Key];
            }

            ViewBag.Events = help;
            ViewBag.Message = TempData["msg"];
            ViewBag.Msg = TempData["por"];
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}